#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

int main()
{
    struct sockaddr_in address;
    int server_fd, client_fd,opt=1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *server_msg = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nHello World!</html>\n";
    server_fd = socket(AF_INET, SOCK_STREAM, 0);
   setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( 8080 );
    bind(server_fd, (struct sockaddr *)&address, sizeof(address));

while(1){
 listen(server_fd, 30);
    client_fd = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);
    read(client_fd , buffer, 1024);
    printf("%s\n",buffer );
    send(client_fd, server_msg, strlen(server_msg) , 0 );
   }
return 0;
}

