#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>

int main()
{
    struct sockaddr_in serv_addr;
    int client_fd = 0;
    char client_msg[20];// = "Hello World <<-- Message from Client\n";
    char buffer[1024] = {0}; //buf
    client_fd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(8054);
    inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr); // Convert $
    connect(client_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)); 
    printf("message for server:  ");
    scanf("%s",client_msg);
    send(client_fd , client_msg , strlen(client_msg) , 0 ); // sending messag$
    read(client_fd, buffer, 1024);    // receiving reply from server
    printf("%s\n",buffer );                  // printing reply
    return 0;
}

